package com.hybris.tdd;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class AppIntegrationTest {

    @Test
    public void main_Will_Execute_Without_Error() throws Exception {
        App app = new App();
        App.main(new String[0]);
        assertThat(app, is(notNullValue()));
    }
}